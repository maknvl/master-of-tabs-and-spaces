
# The Master of Tabs and Spaces Minigame #

Helps to establish dark truths about monospace fonts, unicode characters, your graphical IDE
or text editor, spaces and tabs (unless you already know that darkness, of course).

Read the comments in [master-of-tabs-and-spaces.go](master-of-tabs-and-spaces.go) to begin.
