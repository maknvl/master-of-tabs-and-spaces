
The Badge of The Master of Tabs and Spaces may only be used by the certified
Masters of Tabs and Spaces. It is the ultimate award regarding Tabs and Spaces.

To check if you are worthy to wear the Badge, please play the accompanying
The Master of Tabs and Spaces Programming Game.

The Badge is a DIY item: you have to find a way print it and attach to a badge
base to wear.

The following Unicode sequence constitutes the badge and should be printed
IN MONOSPACE FONT. You should probably `cat` it in the terminal, otherwise
something would look not as nice as it should.

Do not save this file in editors that are too eager to fix linefeeds.
-------------------------------------------------------------------------------

             _________________________________________
                                                      |            |
            | The wearer of this badge is a certified |　          |       MASTER OF TABS AND SPACES　　　　 |
            |_________________________________________|


-------------------------------------------------------------------------------
