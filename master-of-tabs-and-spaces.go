//
// The Master of Tabs and Spaces Minigame
//
// Helps to establish dark truths about monospace fonts, unicode characters,
// your graphical IDE/text editor, spaces and tabs (unless you already know
// that darkness, of course).
//
// Read the source comments to play.
//
// ----------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom
// the Software is furnished to do so, subject to the following conditions:
//
// 1. The above copyright notice and this permission notice shall be included
//    in all copies or substantial portions of the Software.
//
// 2. The game rules contained in the source code of the Software shall not be
//    modified and shall be included in all copies or substantial portions
//    of the Software.
//
// 3. The person obtaining a copy of this software agrees to deem the game
//    rules, described in the source code of the Software, as LEGALLY BINDING,
//    should the person choose to play the game.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
// ----------------------------------------------------------------------------
//
package main

import "fmt"

func main() {
	
	//
	// Please be warned, that you CAN actually lose, and if you do, you have
	// the LEGAL OBLIGATION to abide by the losing conditions. This is SERIOUS!
	//
	// You have only one shot. Doing experiments, research, using internet
	// and asking for outside help is NOT allowed once you read past this point
	// and until you finish the game or decline to play.
	//
	// Here is what you should do:
	//
	// 1. Open this file in your favourite IDE or text editor.
	//
	// 2. Position the "^" character in the const below using only tabs and
	//    spaces so that when printed to console (use your favourite terminal
	//    emulator) it will be exactly below the exclamation mark ("!").
	//    You may only edit the line that has the "^" character.
	//
	// 3. Save the file and do:
	//    go run master-of-tabs-and-spaces.go 
	//
	// If you have successfully placed the marker exactly below the exclamation
	// mark, you can proudly wear the Master of Tabs and Spaces Badge and have
	// the final say in any tabs vs. spaces debate (unless another Master
	// of Tabs and Spaces says otherwise).
	//
	// Should you lose:
	//
	//  1. You are NOT allowed to try again for 2 years.
	//
	//  2. If you participate as a developer in any number of software projects,
	//     that deal with ANY kind of text output, be it to text terminals,
	//     into files, or to other presentation, consumption, data storage or
	//     data transmission means, you have to pay Mikhail A. Konovalov,
	//     the author of the code in this file, 0.01€ (1 euro cent) per day
	//     per each line of code containing tab characters in the data being
	//     output, transmitted or otherwise produced by said software, unless
	//     these tab characters are contained in the input data, which is
	//     transformed by your software into the specified output, transmission
	//     or other production of text output, counted as of 23:59 UTC of the
	//     preceding day, in any of such projects.
	//     Shorter: If your software adds tabs to it's output, you pay me bucks
	//     for each line of code that does so, daily, to mitigate the evil that
	//     tabs do to the world when used unappropriately.
	//
	// If you have read the rules and don't want to play, that is fine,
	// but you are NOT allowed to try playing again for 2 years.
	//

	fmt.Printf(`
   This is a game. To play, you have to read the source code.
    ________________________________________________________
   |                                                        |
   |	 Once upon a time,                                  |
   |	 　   日ぇ櫓人ダート	said: "I like 筋	!"          |
   |                                                        |
                           ^ 
   |________________________________________________________|
    
    `)
}